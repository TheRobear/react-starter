#React Starter

This is a starter project build on React that was not build using create-react-app.

##Getting Started
1. Clone this repository.
2. Open a console in the clone director and type:
>```npm install```
3. have fun!

###To Run The Dev Server
```npm run start```

###To Make A Build
```npm run build```
Builds will be placed in the "dist" folder.

This uses the webpack-dev-server to serve up the React app while developing. I have configured it to run on port 3000. You can edit line 7 of package.json to use whatever port you like.

###Dependencies
This project uses the following dependencies

###Dev Dependencies
Babel Core 7.2.2  
Babel Plugin Proposal Class Properties 7.3.0  
Pabel Preset Env 7.3.1  
Babel Preset React 7.0.0  
Babel Loader 8.0.5  
CSS Loader 2.1.0  
HTML Webpack Plugin 3.2.0  
Style Loader 0.23.1  
Webpack 4.29.0  
Webpack CLI 3.2.1  
Webpack Dev Server 3.1.14  

###Dependencies
React 16.7.0  
React DOM 16.7.0  
React Router Dom 4.3.1  