import React, {Component} from 'react';

class App extends Component {
    state = {
        title: "React Starter Template"
    }

    render() {
        return (
            <div>
                <div>
                    <h1>{this.state.title}</h1>
                </div>
                <div>Edit App.js to get started.</div>
            </div>
        );
    }
}

export default App;